<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Twig Plugin
 *
 * Parses selected content with the Twig templating engine
 *
 * @author		Fredi Bach
 * @copyright	Copyright (c) 2013, getunik AG
 */
class Plugin_Twig extends Plugin
{
	public $version = '1.0.0';

	public $name = array(
		'en'	=> 'Twig'
	);

	public $description = array(
		'en'	=> 'The Twig plugin parses selected content with the Twig templating engine.'
	);

	/**
	 * Returns a PluginDoc array that PyroCMS uses
	 * to build the reference in the admin panel
	 *
	 * All options are listed here but refer
	 * to the Blog plugin for a larger example
	 *
	 * @return array
	 */
	public function _self_doc()
	{
		$info = array(
			'parse' => array(
				'description' => array(// a single sentence to explain the purpose of this method
					'en' => 'Parses the content with Twig'
				),
				'single' => false,// will it work as a single tag?
				'double' => true,// how about as a double tag?
				'variables' => 'data',// optional data to parse
				'attributes' => array()
			)
		);

		return $info;
	}

	/**
	 * parse
	 *
	 * Usage:
	 * {{ twig:parse }}
	 *   {:% set clients = plugin('clients', 'cycle') %:}
	 *   <ul>
	 *   	{:% for entry in clients.entries %:}
     *  		<li>{:{: entry.name :}:}</li>
	 *		{:% endfor %:}
	 *	 </ul>
	 * {{ /twig:parse }}
	 *
	 * {{ twig:parse data="$_SERVER" }}
	 *   {:{: SERVER_ADDR :}:}
	 * {{ /twig:parse }}
	 *
	 * @return string
	 */
	public function parse()
	{
		
		$data = $this->attribute('data', array());
		if ($data != array()) eval('$data = '.$data.';');
		
		$content = $this->content();
		
		require_once 'Twig/Autoloader.php';
		Twig_Autoloader::register();
		
		$loader = new Twig_Loader_String();
		$twig = new Twig_Environment($loader, array('debug' => true));
		
		
		// plugin custom function //
		
		$function = new Twig_SimpleFunction('plugin', function ($name, $method, $attributes) {
		    
		    $plugin_name = 'Plugin_'.ucfirst($name);
		    $plugin_class = new $plugin_name();
		    
		    if ($attributes == null){
			    $attributes = '{}';
		    }
		    
		    $attributes = json_decode($attributes, true);
		    
		    foreach($attributes as $k => $v){
			    $plugin_class->set_attribute($k,$v);
		    }
			
		    $output = call_user_func_array( array( $plugin_class, $method ), $attributes );
		    
		    if (is_array($output) && isset($output[0]) && count($output) == 1){
		    	return $output[0];
		    }
			
			return $output;
		        
		});
		$twig->addFunction($function);
		
		
		// lex custom filter //
		
		$filter = new Twig_SimpleFilter('lex', function ($data) {
	        
	        $parser = new MY_Parser();
	        $data = str_replace('&#123;','{',str_replace('&#125;','}',$data));
	        return $parser->parse_string($data, array(), true, true);
	        
		});
		$twig->addFilter($filter);
		
		
		// orderby custom filter //
		
		$orderbyglobal = '';
		$filter = new Twig_SimpleFilter('orderby_*', function ($orderby, $data) {
		    
	        global $orderbyglobal;
	        $orderbyglobal = explode('_', $orderby);
	        
	        usort($data, function($a, $b)
	        {
	            global $orderbyglobal;
	            return strcmp(strtolower($a[$orderbyglobal[0]]), strtolower($b[$orderbyglobal[0]]));
	        });
	        
	        if (isset($orderbyglobal[1]) && $orderbyglobal[1] == 'desc'){
	            $data = array_reverse($data);
	        }
	        
	        return $data;
	        
		});
		$twig->addFilter($filter);
		
		
		// where is custom filter //
		
		$filter = new Twig_SimpleFilter('where_*_is_*', function ($var, $state, $data) {
		        
	        if ($state == 'false') $state = false;
	        if ($state == 'true') $state = true;
	        
	        $newdata = array();
	        if (count($data) > 0){
	                foreach($data as $k => $v){
	                        if (isset($v[$var]) && $v[$var] == $state){
	                                $newdata[] = $v;
	                        }
	                }
	        }
	        
	        return $newdata;
	        
		});
		$twig->addFilter($filter);
		
		$twig->addExtension(new Twig_Extension_Debug());
		
		$string = str_replace('{:','{',str_replace(':}','}',$this->content()));
		
		return $twig->render($string, $data);
		
	}

}
