<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Twig extends Module
{
    public $version = '1.0.0';

    private $module_name;

    public function __construct()
    {
        parent::__construct();
        $this->module_name = strtolower(str_replace('Module_', '', get_class()));
    }

    public function info()
    {
		$config = array(
            'name' => array(
                'en' => 'Twig'
            ),
            'description' => array(
                'en' => 'Home of the Twig plugin'
            ),
            'backend' => false,
            'frontend' => false,
        );

        return $config;
    }

    public function install()
    {
        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function uninstall()
    {
        return true;
    }
}
