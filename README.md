# Twig module for PyroCMS

Parses selected content with the Twig templating engine.

- version - 1.0.0
- Authors - Fredi Bach - fredi.bach@getunik.com
- Website - [getunik AG](http://www.getunik.com)

## What is it?

The Lex Parser that comes with PyroCMS definitely isn't the strongest part of PyroCMS, I think
everyone can agree on that. It's quite limited in many ways. There are however some very good
teplating engines out there, and probably the best IMHO is [Twig](http://twig.sensiolabs.org/). Sadly there's no way to replace
Lex with Twig using a module, but with this module, there's at least a way to use Twig functionality
with PyroCMS. The PyroCMS Twig module contains a plugin that opens a Twig "area" inside your Lex
templates. Sadly it's not possible to use Twig syntax inside a Lex template, as it confuses the Lex
parser, so we had to change it a little, but it should still be close enough to Twig to be easily 
understandable.

## How does it work?

Here's a very basic example:

```
{{ twig:parse data="$_SERVER" }}
    {:{: SERVER_ADDR :}:}
{{ /twig:parse }}
```

As you can see, the plugin method "parse" is called and the attribute "data" adds all "$_SERVER" variables
to the Twig context. Inside the plugin call, we output the server address with "SERVER_ADDR". As you can see, 
what would look like "{{ SERVER_ADDR }}" in Twig is used as "{:{: SERVER_ADDR :}:}" to not confuse the Lex
parser. Basically, you write your Twig template the same way as before, just replace "{" with "{:" and "}" with
":}".

Here's a more complicated example:

```
{{ twig:parse }}
		
	{:% set clients = plugin('volcano', 'cycle', '{"params": {"namespace": "getunik", "stream": "clients"}}') %:}
	
	<ul>
	{:% for entry in clients.entries %:}
    	<li>{:{: entry.name :}:}</li>
	{:% endfor %:}
	</ul>

{{ /twig:parse }}
```

The new thing here is that we use a custom Twig function to call a PyroCMS plugin, it's called "plugin" and has 
the following parameters:

- Plugin Name
- Plugin Method
- Attributes (supplied as JSON)

We store the plugin output into a new Twig variable. Can you see the advantage already? You can call as many plugins 
as you want. Store as much data in variables as you want. You can than use that data in any way you want, you always have
access to all your data. Make multiple loops inside other loops with different data? No problem.

If you need to use dynamic plugin parameters, you can achive that like this:

```
{:% set params %:}
	{"category": "{:{: clientcategory.id :}:}", "lang": "de"}
{:% endset %:}

{:% set clients = plugin('getunik', 'clients', params) %:}
```

## Custom filters

I added a few additional custom filters to Twig that can be useful. Most of them I initially created for my side project [Fourstatic](http://fourstatic.com), 
but they can be quite useful for Pyro, as well.

### The "lex" filter

Let's start with one you'll definitely need if you have fields that can contain Lex tags, the "lex" filter. Here's how you
use it:

```
{:{: sometext|lex :}:}
```

### The "orderby_*" filter

This one lets you order your array based on a specific field in that array, for example a name field in a contact object:

```
{:% for contact in contacts|orderby_name %:}
	{:{: contact.prename :}:} {:{: contact.name :}:}
{:% endfor %:}
```

You can reverse the sort order like this:

```
{:% for contact in contacts|orderby_name_desc %:}
	{:{: contact.prename :}:} {:{: contact.name :}:}
{:% endfor %:}
```

Of course this custom filter is only useful if you can't get the data already ordered from the template or if you need
the same data twice, but with different ordering, as that will save you a call to the plugin.

### The "where_*_is_*" filter

With this filter you can filter data by it's fields content like this:

```
{:% for contact in contacts|where_sex_is_male %:}
	{:{: contact.prename :}:} {:{: contact.name :}:}
{:% endfor %:}
```

Again, only use it if you can't get that data any other way or if you use the same data twice.

## What next?

Currently we are "just" parsing Twig inside a plugin context, but the idea is to go further in a future release, 
with the possibility to use a template directory where you can use all the other cool Twig features, like template
inheritance, marcros, includes ...

Till than, we have another small module that adds template inhertitance, so have a look: [Layout Module](https://bitbucket.org/getunik/pyrocms-layout-module)